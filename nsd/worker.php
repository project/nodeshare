<?php

// Set this to your MySQL login credentials
$SQL_HOST = 'localhost';
$SQL_USER = 'my_nodeshare';
$SQL_PASS = 'secret123';
$SQL_DB = 'nodeshare';

// Set this to whatever you want your log file to be.
$fp = fopen('nsd.log', 'a');

while (1) {
  unset($result, $db);
  $db = mysql_connect($SQL_HOST, $SQL_USER, $SQL_PASS);
  mysql_select_db($SQL_DB, $db);
  $result = mysql_query("SELECT * FROM queue WHERE sent=0");
  $dberr = mysql_error($db);
  if ($dberr) {
    print '[Error] ' . $dberr . "\n";
  }
  while ($row = mysql_fetch_object($result)) {
    $dest_url = 'http://' . $row->destination . '/nodeshare/post';
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $dest_url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 900);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'json='.urlencode($row->json));
    $ret = curl_exec($ch);
    $err = curl_error($ch);
    
    if ($err) {
      write_log('Request ' . $row->qid . ' sent to ' . $row->destination . ' failed with cURL error: ' . $err);
    } else {
      if (strpos(strtolower($ret), 'success') === false) {
        write_log('Remote site ' . $row->destination . ' rejected request ' . $row->qid . ' with message: ' . $ret);
      } else {
        write_log('Node ' . $row->qid . ' accepted by remote site ' . $row->destination);
        mysql_query("UPDATE queue SET sent=1 WHERE qid=" . $row->qid);
      } 
    }
    
    // Sleep 3/10 of a second between requests
    usleep(300000);
  }
  mysql_close($db);
  // Sleep 30 seconds between processing loops
  sleep(30);
}

function write_log($text) {
  global $fp;
  $prefix = "[" . date('Y-m-d H:i:s') . "] ";
  fwrite($fp, $prefix . $text . "\n");
}