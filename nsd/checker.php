<?php

// Set this to whatever the path to your worker script is.
// Absolute paths are preferred.
$NSD_PATH = "/root/nsd/worker.php";

// Set this to the path to your PHP exectuable. If you don't have one, you must install it.
$PHP_PATH = "/usr/bin/php";

$ps=`ps aux | grep worker.php | grep -v grep`;
$a = sscanf($ps, "%s %d");
$stat = $a[1];
if (!$stat) {
  $pid = pcntl_fork();
  if ($pid == -1) {
    die('Fork error (weird)');
  } else if ($pid) {
    // parent
    pcntl_wait($status);
  } else {
    $args = array('-q', $NSD_PATH);
    pcntl_exec($PHP_PATH, $args);
  }
}


?>