Nodeshare -- a module to share Drupal nodes between sites using HTTP.

OVERVIEW
--------------------
The purpose of Nodeshare is to replicate a node object on another Drupal site
using only HTTP. Of course, this sort of content syndication can be done in a
lot of ways. You could produce an RSS feed on the sending site, for example, or
you could do some sort of database black magic if your sites live on the same
server. However, the goal here is to replicate the node object lock, stock and
barrel on the remote site, including taxonomy and CCK fields, if applicable, and
to do it without any assumptions about where the sites are or how they are
related to one another. For that, RSS feeds and other more generalized methods
of content syndication might not do the trick.

This module was originally developed to address the unique situation on the
Observer.com and Politicker.com network of sites, where we needed a low-level
replication of nodes, including taxonomy and CCK fields that existed on both the
origin and destination site within different content types.

Nodeshare works by encoding a Drupal node object into a JSON string and using an
HTTP POST request to send it to a remote site. It is designed to be at least
somewhat scalable, and so when a node is being shared to, for example, 10 remote
sites in a single request, it is not optimal to wait for all 10 HTTP requests to
succeed or fail sequentially before proceeding, nor is it optimal to stack all
those requests as fast as possible in the event that all 10 remote sites on the
same server. Instead, nodeshare saves requests into a queue table, and external
daemon runs and processes queue requests. A small delay is imposed between
requests to prevent unnecessarily taxing remote servers or local resources.

REQUIREMENTS
--------------------
You will need to have a solid working knowledge of PHP and the Drupal API to
make use of this module. In fact, just enabling the module won't even have any
affect out of the box. To leverage the functionality of nodeshare, you will need
to either edit the module yourself or write a companion module that calls the
nodeshare_send_node() function. The latter option is obviously preferable.
Nodeshare defines its own hook, hook_nodeshare, which allows your companion
module to react to nodeshare transactions.

The rationale for doing it this way was to avoid tying the infrastructure to a
very specific use case. For us, we have two very different sites that needed
different treatment. Rather than write two disparate modules, it made more sense
to write a common core and smaller add-on modules to customize it. The core is
nodeshare.

Other requirements:
  - Drupal 5.x (there is no 6.x version for the time being)
  - PHP 5 or better with the cURL and JSON extensions installed
  - Command line PHP access. On some package management systems this must be
  installed separately from the Apache PHP module (e.g., the php5-cli package in
  Debian's apt-get system)
  - Access to run the nodeshare queue daemon. This is written in PHP (hence the
  PHP-CLI requirement), and would ideally be run using cron. Inquire with your
  hosting provider as to whether you have this access if you are unsure.
 
USING NODESHARE
--------------------
Please read INSTALL.txt for instructions on how to set up the nodeshare queue
daemon. Nodeshare will not work without it. For a simple demo on how to write a
companion module for nodeshare, check out the example_nodeshare.module file
packaged here.

Two key points to keep in mind when theming nodeshare nodes are:
  - The function nodeshare_origin_url($nid) will return the link-ready URL to the
  original node. This can be passed through l() to produce links to the original
  node, if need be.
  - The convention for checking to see whether a node is local or nodeshare'd is
  to check if ($node->nodeshare_source) { ... }
 
AUTHOR
--------------------
The module is developed and maintained by Matt Johnson (mjohnson@observer.com;
xmattus on drupal.org).

Development has been made possible by support from the Observer Media Group.